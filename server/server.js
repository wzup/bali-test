require('babel-core/register')({
    presets: ['es2015', 'react'],
});



var loopback = require('loopback');
var boot = require('loopback-boot');
var adaro = require('adaro');
var path = require('path');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var app = module.exports = loopback();

app.start = function() {
    // start the web server
    return app.listen(function() {
        app.emit('started');
        console.log('Web server listening at: %s', app.get('url'));
    });
};

app.set('views', path.resolve(__dirname, '../views'));
// app.engine('dust', require('dustjs-linkedin').render);
app.engine('dust', adaro.dust({}));
app.set('view engine', 'dust');

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
    if (err) throw err;

    // start the server if `$ node server.js`
    if (require.main === module)
        app.start();
});
