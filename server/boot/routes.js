'use strict';

var http = require('http');
var React = require('react');
var ReactDOM = require('react-dom/server');
var components = require('../../client/js/components.js');
var btn_component = require('../../client/js/btn-component.js');
var apiKey = 'api-key=a04cf0ec99b44d4b44bb72b746847551%3A11%3A73547996';


module.exports = function(app) {
    var User = app.models.user;
    var aToken = app.models.AccessToken;

    // Install a "/ping" route that returns "pong"
    app.get('/ping', function(req, res) {
        res.send('pong');
    });

    app.get('/login', function(req, res) {
        var Form = React.createFactory(components.Form);
        // console.log('== login GET req.cookies ==: ', req.cookies);
        if (req.cookies.userId) {
            // console.log('== login GET ЕСТЬ req.cookies.userId ==: ', req.cookies.userId);
            return res.redirect('/users/' + req.cookies.userId);
        } else {
            // console.log('== login GET НЕТ req.cookies.userId ==: ', req.cookies.userId);
            res.render('login', {
                react: ReactDOM.renderToString(Form({
                    login: "Залогениться"
                })),
                title: "Логин"
            });
        }
    });

    app.post('/login', function(req, res) {
        // console.log('== req.body ==: ', req.body);
        // console.log('== req.body.username ==: ', req.body.username);
        // console.log('== req.body.password ==: ', req.body.password);
        User.login({
            username: req.body.username,
            password: req.body.password
        }, 'user', function(err, token) {
            if (err) {
                res.statusCode = 401;
                return res.redirect('/wrong');
            }
            res.clearCookie('userId');
            res.cookie('userId', token.userId);
            res.redirect('/users/' + token.userId);
        });
    });

    app.get('/wrong', function(req, res) {
        res.render('wrong');
    });

    app.get('/needlogin', function(req, res) {
        res.render('needlogin');
    });

    app.get('/books', function(req, res) {
        var url = 'http://api.nytimes.com/svc/books/v3/lists/hardcover-fiction.json?' + apiKey;
        var PostBox = React.createFactory(components.PostBox);
        var body = '';

        http.get(url, function(httpResp) {

            httpResp.setEncoding('utf8');
            httpResp.on('data', function(chunk) {
                body += chunk;
            });

            httpResp.on('end', function() {
                // console.log('== body ==: ', body);
                res.render('books', {
                    react: ReactDOM.renderToString(PostBox({
                        data: JSON.parse(body).results.books
                        // data: JSON.parse(body)
                    })),
                    title: "Книги"
                });
            });

        }).on('error', function(err) {
            res.statusCode = 500;
            console.error('== Books NYT request ERROR ==: ', err);
            res.end('ERROR! Error hapenned when requesting providers API. Wait and repeat your request.');
        });
    });

    app.get(/\/books\/(\w{10})$/i, function(req, res) {
        var url = 'http://api.nytimes.com/svc/books/v3/lists.json?list-name=Hardcover+Fiction&isbn={isbn}&' + apiKey;
        var parsedUrl = url.replace(/\{isbn\}/, req.params[0]);
        var body = '';
        var BookBox = React.createFactory(components.BookBox);

        http.get(parsedUrl, function(httpResp) {

            httpResp.setEncoding('utf8');
            httpResp.on('data', function(chunk) {
                body += chunk;
            });

            httpResp.on('end', function() {
                var jsonBody = JSON.parse(body);

                res.render('book', {
                    react: ReactDOM.renderToString(BookBox({
                        jsonBody: jsonBody
                    })),
                    title: "Книга"
                });
            });

        }).on('error', function(err) {
            res.statusCode = 500;
            console.error('== ONE Book NYT request ERROR ==: ', err);
            res.end('ERROR! Error hapenned when requesting providers API. Wait and repeat your request.');
        });
    });

    app.get(/\/users\/([0-9]+)$/i, function(req, res) {
        if (!req.cookies.userId) {
            return res.redirect('/needlogin');
        }
        else if (req.cookies.userId) {
            if(req.cookies.userId == req.params[0]) {
                // console.log('== USER/id СОВПАЛИ ==: ');
                return res.render('profile');
            }
            else if(req.cookies.userId != req.params[0]) {
                // console.log('== USER/id НЕ СОВПАЛИ ==: ');
                return res.render('profile_stray');
            }
        }
    });

    app.get('/', function(req, res) {
        // console.log("=== Cookies ===: ", req.cookies);
        res.render('index', {
            'hello': 'Привет',
            'name': 'Bali!',
            'title': 'CodeTest - index page'
        });
    });
};
