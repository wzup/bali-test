'use strict';

var server = require('../server');
var ds = server.dataSources.db;
var userModel = server.models.user;
var Role = server.models.Role;
var RoleMapping = server.models.RoleMapping;
// console.log('== server ==: ', server.models);

var collections = ['user'];

ds.automigrate(collections, function(er) {
    if (er) throw er;
    console.log('== Looback tables [' + collections + '] created in ',
        ds.adapter.name);

    server.models.user.create([{
            username: 'admin',
            password: "admin",
            email: 'admin@gmail.com',
            // emailVerified: true,
            dtCrt: new Date(),
            dtUpd: new Date()
        }, {
            username: 'admin2',
            password: "admin2",
            email: 'admin2@gmail.com',
            // emailVerified: true,
            dtCrt: new Date(),
            dtUpd: new Date()
        }
        /*, {
                username: 'admin3',
                password: "admin3",
                email: 'admin3@gmail.com',
                emailVerified: true,
                dtCrt: new Date()
            }*/
    ], function(err, newUsers) {
        if (err) {
            console.error("== Create ERROR ==:\n\r", err);
            ds.disconnect();
            process.exit(1);
        }
        console.log('== newUsers ==:\n\r', newUsers);

        userModel.findOne({
            email: 'admin@gmail.com'
        }, function(err, user) {
            if (err)
                throw err;

            console.log('== find user ==: ', user);

            Role.findOne({
                name: "admin"
            }, function(err, model) {
                if (err) {
                    throw err
                };

                if (!model) {
                    //create the admin role
                    Role.create({
                        name: 'admin'
                    }, function(err, role) {
                        if (err) throw err;

                        console.log('== Created role: ==\n\r', role);

                        //make user an admin
                        role.principals.create({
                                principalType: RoleMapping.USER,
                                principalId: user.id
                            },
                            function(err, principal) {
                                if (err) {
                                    throw err;
                                }

                                console.log('== Created principal: ==:\n\r', principal);
                                ds.disconnect();
                                // process.exit();
                            });
                    });
                } else {
                    console.log('== Role admin exists. Exit. ==: ');
                    ds.disconnect();
                    // process.exit();
                }
            });
        });
    });
});
