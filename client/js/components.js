var isNode = typeof module !== 'undefined' && module.exports,
    React = isNode ? require('react') : window.React,
    ReactDOM = isNode ? require('react-dom') : window.ReactDOM;

var Form = React.createClass({

    onSubmit: function(e) {},
    onClick: function(e) {
        console.log('== click e ==: ', e);
    },
    render: function() {
        return (
            React.DOM.form({
                    action: '/login',
                    method: 'post',
                    id: 'login-form',
                    onSubmit: this.onSubmit
                },
                React.DOM.div(null, React.DOM.input({
                    type: "text",
                    id: "username",
                    name: "username",
                    placeholder: "Логин"
                })),
                React.DOM.div(null, React.DOM.input({
                    type: "password",
                    id: "password",
                    name: "password",
                    placeholder: "Пароль"
                })),
                React.DOM.button({
                    id: "sbm-btn",
                    // onClick: this.onClick
                }, this.props.login)
            )
        );
    }
});

var PostBox = React.createClass({
    render: function() {
        return (
            <div className="postBox" >
                <PostList data={this.props.data} />
            </div>
        );
    }
});

var PostList = React.createClass({

    render: function() {
        var postNodes = this.props.data.map(function(post, i) {
            return (
                <Post key={i}
                    title={post.title}
                    isbn10={post.primary_isbn10}
                    author={post.author}
                    rank={post.rank}
                    desc={post.description}/>
            );
        });
        return (
            <div className="postList">
                {postNodes}
            </div>
        );
    }
});

var Post = React.createClass({
    render: function() {
        var href = 'books/' + this.props.isbn10;
        return (
            <div className="post">
                <div>Title: <a href={href}>{this.props.title}</a></div>
                <div>Author: {this.props.author}</div>
                <div>Description: {this.props.desc}</div>
                <div>Rank: {this.props.rank}</div>
                <div>ISBN10: {this.props.isbn10}</div>
                {this.props.children}
            </div>
        );
    }
});

var BookBox = React.createClass({
    render: function() {
        return (
            <div>
                <Book jsonBody={this.props.jsonBody} />
            </div>
        );
    }
});

var Book = React.createClass({
    render: function() {
        var href = this.props.jsonBody.results[0].amazon_product_url;
        return (
            <div className="post">
                <div>Title: {this.props.jsonBody.results[0].book_details[0].title}</div>
                <div>Author: {this.props.jsonBody.results[0].book_details[0].author}</div>
                <div>Description: {this.props.jsonBody.results[0].book_details[0].description}</div>
                <div>Publisher: {this.props.jsonBody.results[0].book_details[0].publisher}</div>
                <div>Amazon URL: <a href={href} target="_blank">See Book on Amazon</a></div>
                <div>Rank: {this.props.jsonBody.results[0].rank}</div>
                <div>ISBN10: {this.props.jsonBody.results[0].book_details[0].primary_isbn10}</div>
            </div>
        );
    }
});



if (isNode) {
    exports.Form = Form;
    exports.PostBox = PostBox;
    exports.PostList = PostList;
    exports.Post = Post;
    exports.BookBox = BookBox;
} else {
    // ReactDOM.render(<Form login="Залогениться" />, document.getElementById('react-form'));
    if (document.getElementById('react-form'))
        ReactDOM.render(React.createFactory(Form)({
            login: "Залогениться"
        }), document.getElementById('react-form'));
    if (document.getElementById('react-news'))
        ReactDOM.render(<PostBox />, document.getElementById('react-news'));
    if (document.getElementById('react-book'))
        ReactDOM.render(<BookBox />, document.getElementById('react-book'));
}
