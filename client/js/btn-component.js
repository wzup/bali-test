'use strict';

var isNode = typeof module !== 'undefined' && module.exports;
var React = isNode ? require('react') : window.React;
var DOM = React.DOM;
// var ReactDOM = require('react-dom');

// var Button = React.createClass({
//     doClick: function () {
//         console.log('== I\'m Clicked ==: ');
//     },
//     render: function() {
//         return (
//             <button id="btn" onClick={this.doClick}>$1 000 000 000</button>
//         );
//     }
// });

var Button = React.createClass({
    doClick: function() {
        console.log('== I\'m Clicked ==: ');
    },
    render: function() {
        return (
            DOM.button({
                id: "btn",
                onClick: this.doClick
            }, "$1 000 000 000")
        );
    }
});

if (isNode) {
    module.exports.Button = Button;
} else {
    ReactDOM.render(React.createFactory(Button)(), document.getElementById('react-button'));
}
